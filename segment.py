from pathlib import Path
from collections import defaultdict

from pybo import BoTokenizer


def mark_skrt_nonwords(tokens):
    out = []
    for t in tokens:
        token = t.content  # .replace(' ', '_')
        if t.affix:
            token = '–'+token

        skrt = False
        nonword = False
        if t.pos == 'oov' or t.pos == 'non-word':
            nonword = True
        if t.skrt:
            skrt = True

        if nonword and not skrt:
            out.append(f'#{token}')
        elif nonword and skrt:
            out.append(f'${token}')
        else:
            out.append(token)
    return out


def extract_syls(token):
    if token.syls:
        return [syl + '་' for syl in token.cleaned_content.strip('་').split('་')]
    else:
        return [token.content]


def syllabled_tokens(tokens):
    out = []
    for t in tokens:
        syls = extract_syls(t)
        if t.affix:
            syls[0] = '–' + syls[0]

        # syls = [s.replace(' ', '_') for s in syls]
        out.extend(syls)

    return out


def tokenize_dir(indir, outdir, frm_func):
    dirname = Path(indir)
    files = dirname.glob('*.txt')
    for f in files:
        content = f.read_text()
        tokens = tok.tokenize(content)
        tokens = frm_func(tokens)
        Path(Path(outdir) / f.name.replace('raw', 'seg')).write_text(' '.join(tokens))


def tokenize_file(filename, outdir, frm_func):
    filename = Path(filename)
    outdir = Path(outdir)
    if not outdir.is_dir():
        outdir.mkdir(exist_ok=True)
    content = filename.read_text()
    tokens = tok.tokenize(content)
    tokens = frm_func(tokens)
    Path(outdir / filename.name.replace('raw', 'seg')).write_text(' '.join(tokens))


def generate_types(filename, outdir):
    filename = Path(filename)
    content = filename.read_text()
    tokens = tok.tokenize(content)
    tokens = mark_skrt_nonwords(tokens)
    types = defaultdict(int)
    for t in tokens:
        types[t.rstrip('་')] += 1

    types = [(v, k) for k, v in types.items()]
    types = sorted(types, reverse=True)

    Path(Path(outdir) / filename.name.replace('raw', 'types')).write_text('\n'.join([f'{a},{b}' for a, b in types]))



if __name__ == '__main__':
    tok = BoTokenizer('noGMD', toadd_filenames=['to_add.txt'], todel_filenames=['to_remove.txt'])
    # tokenize_dir('raw_texts/stories_raw', 'output/segmented/stories_raw', mark_skrt_nonwords)
    # tokenize_file('raw_texts/stories_raw/T340_raw-1-.txt', 'output/syllabled/stories_raw', syllabled_tokens)
    tokenize_file('raw_texts/spyod pas rnam sbyong.txt', 'output/', mark_skrt_nonwords)
    # generate_types('raw_texts/stories_raw/T340_raw-1-.txt', 'output')
    # generate_types('raw_texts/T340_raw.txt', 'output')
