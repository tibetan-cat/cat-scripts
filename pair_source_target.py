from pathlib import Path
import argparse


def find_file_pairs(in_path):
    """

    :param in_path: OmegaT project path
    :return: list of tuples of source/target Path objects. source files without target file are ignored
    """
    project_path = Path(in_path)
    source_path = project_path / 'source'
    target_path = project_path / 'target'

    pairs = []
    for sf in source_path.glob('*.txt'):
        for tf in target_path.glob('*.txt'):
            if sf.stem == tf.stem:
                pairs.append((sf, tf))

    return pairs


def pair_source_target(source, target):
    """

    :param source: Path to source text
    :param target: Path to target text
    :return: str with aligned source/target
    """
    s_segments = source.read_text().split('\n\n')
    t_segments = target.read_text().split('\n\n')
    assert len(s_segments) == len(t_segments)

    paired = [[s, t] for s, t in zip(s_segments, t_segments)]

    for num, p in enumerate(paired):
        if p[0] == p[1]:
            paired[num][1] = '— ' * 10
        # replace ticks by tseks
        p[0] = p[0].replace('-', '་')

    out = '\n\n'.join(['\n'.join(p) for p in paired])

    return out


def create_dir_structure(path):
    """Creates parent dirs in the given path

    :param path: path to parse
    """
    if path.suffix:  # needs to ignore file. (assumes files have suffixes)
        parts = list(path.parts)[:-1]
    else:
        parts = list(path.parts)

    current = Path()
    for p in parts:
        current = current / p
        if not current.is_dir():
            current.mkdir(exist_ok=True)


def main(project_path):
    # find source/target pairs in the project
    pairs = find_file_pairs(project_path)

    for s, t in pairs:
        print(f'{s.name}')
        # pair source and target texts
        paired = pair_source_target(s, t)

        # write paired files
        out_file = Path('output') / s.parts[-3] / s.parts[-1]
        create_dir_structure(out_file.parent)
        out_file.write_text(paired)


parser = argparse.ArgumentParser()
parser.add_argument('--project', help='path to the OmegaT project to be parsed')


if __name__ == '__main__':
    args = parser.parse_args()
    project_path = args.project

    if project_path:
        main(project_path)

    else:
        raise SyntaxError('no OmegaT project path provided. Exiting.')
