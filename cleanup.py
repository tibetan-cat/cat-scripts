from pathlib import Path


def clean_derge_katen(str_list):
    def find_markup_end(line):
        a = line.rfind(']')
        b = line.rfind('}')
        if a > b:
            return a
        else:
            return b
    out = []
    for line in str_list:
        start = find_markup_end(line)
        if start == -1:
            out.append(line)
        else:
            out.append(line[start + 1:])
    return ''.join(out)


def clean(filename, func):
    filename = Path(filename)
    lines = [line.strip('\n') for line in filename.open()]
    cleaned = func(lines)
    Path(filename.parent / str(filename.stem + '_raw' + filename.suffix)).write_text(cleaned)


if __name__ == '__main__':
    clean('raw_texts/T340.txt', clean_derge_katen)
