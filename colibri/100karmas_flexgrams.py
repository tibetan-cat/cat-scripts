from pathlib import Path
from collections import defaultdict, OrderedDict

import colibricore


def prepare_corpus(corpus_files, name):
    plaintext = name + '.txt'
    classfile = name + '.colibri.cls'
    encodedcorpus = name + '.colibri.dat'

    if not Path(plaintext).is_file():
        with Path(plaintext).open('w') as ptxt:
            for f in sorted(Path(corpus_files).glob('*.txt')):
                ptxt.write(f.read_text() + '\n')

    classencoder = colibricore.ClassEncoder()
    classencoder.build(plaintext)
    if not Path(classfile).is_file():
        classencoder.save(classfile)

    if not Path(encodedcorpus).is_file():
        classencoder.encodefile(plaintext, encodedcorpus)

    classdecoder = colibricore.ClassDecoder(classfile)

    return classencoder, classdecoder, {'plaintext': plaintext, 'classfile': classfile, 'encodedcorpus': encodedcorpus}


def whole_corpus(encodedcorpus):
    """Corpus is made of sentences (pattern instances)

    Methods:
        .sentencecount(): size of corpus. note: use range(corpus.sentencecount()) to loop over sentence idx
        .sentencelength(x): amount of tokens in sentence x

        .findpattern(pattern): allows to loop with `for (sentence, token), pattern in corpus.findpattern(pattern):`
                sentence: sentence idx
                token: token idx (in sentence)

        indices:
                corpus[(sentence, token)]: returns the token found at (sentence, token)

        slices:
                corpus[(sentence, token):(sentence, token)]: returns the tokens.
                note: limited to tokens in a single sentence

    """
    return colibricore.IndexedCorpus(encodedcorpus)


def pattern(classencoder, string):
    """tokens must already exist in the classencoder and be separated by spaces

    Pattern methods:
        ngrams:
                .ngrams(): generates all ngrams, from unigrams until len(tokens) - 1
                .ngrams(x): generates all x-grams
                .ngrams(x, y): generates all ngrams from x to y

        in:
                pattern1 in pattern2
                token in pattern

        tostring:
                .tostring(classdecoder): returns the corresponding string

    :return: a Pattern object. contains Token objects
    """
    return classencoder.buildpattern(string)


def unindexed_model(modelname, encodedcorpus, mintokens=2, maxlength=8):
    modelname += '.colibri.unindexed-patternmodel'
    options = colibricore.PatternModelOptions(mintokens=mintokens, maxlength=maxlength)

    if not Path(modelname).is_file():
        model = colibricore.UnindexedPatternModel()
        model.train(encodedcorpus, options)
        model.write(modelname)

    else:
        model = colibricore.UnindexedPatternModel(modelname, options)

    print(modelname + ' has', len(model), ' patterns.')
    return model


def indexed_model(modelname, encodedcorpus, mintokens=2, maxlength=8):
    """
    Methods:
            .coverage(pattern): how many of the tokens in the original data are covered by the given pattern

            .tokens(): total amount of tokens in corpus data

            .types(): total amount of word types in corpus data

    """
    modelname += '.colibri.patternmodel'
    options = colibricore.PatternModelOptions(mintokens=mintokens, maxlength=maxlength)

    if not Path(modelname).is_file():
        model = colibricore.IndexedPatternModel()
        model.train(encodedcorpus, options)
        model.write(modelname)

    else:
        model = colibricore.IndexedPatternModel(modelname, options)

    print(modelname + ' has', len(model), ' patterns.')
    return model


def reverseindexed_model(modelname, encodedcorpus, mintokens=2, maxlength=8):
    """
    Methods:
            .getreverseindex((sentence, token)): find pattern at (sentence, token)

            .getreverseindex_bysentence(x): get all patterns ordered by starting index in sentence x

            .reverseindex(): to loop over all indices in the model

            .items(): to loop over (ref, pattern) over all the model
    """
    modelname += '.colibri.reverse-patternmodel'
    options = colibricore.PatternModelOptions(mintokens=mintokens, maxlength=maxlength)

    corpus = colibricore.IndexedCorpus(encodedcorpus, reverseindex=True)
    model = colibricore.IndexedPatternModel(reverseindex=corpus)
    if not Path(modelname).is_file():
        model.train(encodedcorpus, options)
        model.write(modelname)

    else:
        model = colibricore.IndexedPatternModel(modelname, options)

    print(modelname + ' has', len(model), ' patterns.')
    return model


def skipgram_model(modelname, encodedcorpus, mintokens=1000, maxlength=8):
    modelname += '.colibri.skipmodel'
    options = colibricore.PatternModelOptions(mintokens=mintokens, maxlength=maxlength, doskipgrams=True)

    corpus = colibricore.IndexedCorpus(encodedcorpus)
    model = colibricore.IndexedPatternModel(reverseindex=corpus)
    if not Path(modelname).is_file():
        model.train(encodedcorpus, options)
        model.write(modelname)

    else:
        model = colibricore.IndexedPatternModel(modelname, options, reverseindex=corpus)

    print(modelname + ' has', len(model), ' patterns.')
    return model


def gramloop(model, mode='flex'):
    """

    :param model:
    :param mode:
    :return:
    """
    if mode == 'flex':
        gramtype = colibricore.Category.FLEXGRAM
    elif mode == 'skip':
        gramtype = colibricore.Category.SKIPGRAM
    else:
        raise ValueError('wrong mode: only accepts "flex" and "skip"')

    instances = {pattern: list(indices) for pattern, indices in model.items() if pattern.category() == gramtype}
    instances = OrderedDict(sorted(instances.items(), key=lambda x: len(x[1]), reverse=True))
    return instances


def is_interesting_skipgram(pattern, mode='flex'):
    """Does not contain more tibetan punctuation than content
    """
    skips = 0
    parts_content = 0
    parts_punct = 0
    parts_total = 0

    to_ignore = ['དང་', 'དང',
                 # all affixed particles
                 '–ར་', '–འི་', '–ས་', '–འམ་', '–འང་',
                 '–ར', '–འི', '–ས', '–འམ',
                 "གི", "ཀྱི", "གྱི", "ཡི",
                 "གི་", "ཀྱི་", "གྱི་", "ཡི་",
                 "གིས", "ཀྱིས", "གྱིས", "ཡིས",
                 "གིས་", "ཀྱིས་", "གྱིས་", "ཡིས་",
                 "སུ", "རུ", "ཏུ", "ན", "ལ", "དུ",
                 "སུ་", "རུ་", "ཏུ་", "ན་", "ལ་", "དུ་",
                 "སྟེ", "ཏེ", "དེ",
                 "སྟེ་", "ཏེ་", "དེ་",
                 "ཀྱང", "ཡང",
                 "ཀྱང་", "ཡང་",
                 "གམ", "ངམ", "དམ", "ནམ", "བམ", "མམ", "འམ", "རམ", "ལམ", "སམ", "ཏམ",
                 "གམ་", "ངམ་", "དམ་", "ནམ་", "བམ་", "མམ་", "འམ་", "རམ་", "ལམ་", "སམ་", "ཏམ་",
                 "པ", "བ",
                 "པ་", "བ་",
                 "པོ", "བོ",
                 "པོ་", "བོ་",
                 "དོ", "བོ", "འོ", "ཏོ",
                 "དོ་", "བོ་", "འོ་", "ཏོ་"]

    for p in pattern.parts():
        string = p.tostring(decoder)

        puncts = ['།', '_']
        if [True for a in puncts if a in string]:
            parts_punct += 1

        elif string not in to_ignore:
            parts_content += 1

        parts_total += 1

    if mode == 'flex':
        return parts_content >= parts_total // 2
    if mode == 'skip':
        # find how many skips in pattern
        for _, s in pattern.gaps():
            skips += s
        return skips < parts_content + parts_punct
    else:
        raise ValueError('either "flex" or "skip"')


def get_skipflex_types(gram, indices, model):
    gram_types = defaultdict(int)
    for index in indices:
        instance = model.getinstance(index, gram)
        gram_types[instance] += model.occurrencecount(instance)

    return OrderedDict(sorted(gram_types.items(), key=lambda t: t[1], reverse=True))


def get_instances_types(instances, mode='freq'):
    types = defaultdict(list)
    for location, gram in instances:
        types[gram].append(location)

    if mode == 'freq':
        for a, b in types.items():
            types[a] = len(b)

        return OrderedDict(sorted(types.items(), key=lambda x: x[1], reverse=True))
    elif mode == 'idx':
        return OrderedDict(sorted(types.items()))
    else:
        raise ValueError('only accepts "freq" and "idx".')


if __name__ == '__main__':
    name = '100karmas_seg'
    corpus_path = '../output/segmented/stories_raw'

    # 1. Prepare corpus
    encoder, decoder, names = prepare_corpus(corpus_path, name)

    # 2. build skipgram model
    skip = skipgram_model(name + '_5', names['encodedcorpus'], 2, 18)

    # 3. Compute flexgrams
    flex = skip.computeflexgrams_fromskipgrams()

    out = Path('flexgrams_most_frequents.txt').open('w')
    max = 3
    c = 0
    for pattern, indices in gramloop(skip, mode='flex'):

        print(f'{len(indices)} {pattern.tostring(decoder)}')
        if is_interesting_skipgram(pattern):
            if c == max:
                break
            c += 1
            out.write(f'{len(indices)} {pattern.tostring(decoder)}\n')
            parts = [p.tostring(decoder) for p in pattern.parts()]
            count = 0
            instances = []
            for index in indices:
                # loop over the patterns found at the given index
                for ppattern in skip.getreverseindex(index):
                    if ppattern.category() == colibricore.Category.SKIPGRAM:
                        pparts = [p.tostring(decoder) for p in ppattern.parts()]
                        if parts == pparts:
                            instances.append((index, skip.getinstance(index, ppattern)))
                            count += 1

            types = get_instances_types(instances)
            out.write('\t' + '\n\t'.join([f'{a.tostring(decoder)} {b}' for a, b in types.items()]) + '\n')

    out.close()
