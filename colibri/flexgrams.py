from pathlib import Path
from collections import defaultdict, OrderedDict
from operator import itemgetter
from pathos.multiprocessing import Pool

import colibricore


def prepare_corpus(corpus_files, name):
    plaintext = name + '.txt'
    classfile = name + '.colibri.cls'
    encodedcorpus = name + '.colibri.dat'

    if not Path(plaintext).is_file():
        with Path(plaintext).open('w') as ptxt:
            for f in sorted(Path(corpus_files).glob('*.txt')):
                ptxt.write(f.read_text() + '\n')

    classencoder = colibricore.ClassEncoder()
    classencoder.build(plaintext)
    if not Path(classfile).is_file():
        classencoder.save(classfile)

    if not Path(encodedcorpus).is_file():
        classencoder.encodefile(plaintext, encodedcorpus)

    classdecoder = colibricore.ClassDecoder(classfile)

    return classencoder, classdecoder, {'plaintext': plaintext, 'classfile': classfile, 'encodedcorpus': encodedcorpus}


def skipgram_model(modelname, encodedcorpus, mintokens=1000, maxlength=8):
    modelname += '.colibri.skipmodel'
    options = colibricore.PatternModelOptions(mintokens=mintokens, maxlength=maxlength, doskipgrams=True)

    corpus = colibricore.IndexedCorpus(encodedcorpus)
    model = colibricore.IndexedPatternModel(reverseindex=corpus)
    if not Path(modelname).is_file():
        model.train(encodedcorpus, options)
        model.write(modelname)

    else:
        model = colibricore.IndexedPatternModel(modelname, options, reverseindex=corpus)

    print(modelname + ' has', len(model), ' patterns.')
    return model


def is_interesting_skipgram(pattern, mode='flex'):
    """Does not contain more tibetan punctuation than content
    """
    skips = 0
    parts_content = 0
    parts_punct = 0
    parts_total = 0

    to_ignore = ['དང་', 'དང',
                 # all affixed particles
                 '–ར་', '–འི་', '–ས་', '–འམ་', '–འང་',
                 '–ར', '–འི', '–ས', '–འམ',
                 "གི", "ཀྱི", "གྱི", "ཡི",
                 "གི་", "ཀྱི་", "གྱི་", "ཡི་",
                 "གིས", "ཀྱིས", "གྱིས", "ཡིས",
                 "གིས་", "ཀྱིས་", "གྱིས་", "ཡིས་",
                 "སུ", "རུ", "ཏུ", "ན", "ལ", "དུ",
                 "སུ་", "རུ་", "ཏུ་", "ན་", "ལ་", "དུ་",
                 "སྟེ", "ཏེ", "དེ",
                 "སྟེ་", "ཏེ་", "དེ་",
                 "ཀྱང", "ཡང",
                 "ཀྱང་", "ཡང་",
                 "གམ", "ངམ", "དམ", "ནམ", "བམ", "མམ", "འམ", "རམ", "ལམ", "སམ", "ཏམ",
                 "གམ་", "ངམ་", "དམ་", "ནམ་", "བམ་", "མམ་", "འམ་", "རམ་", "ལམ་", "སམ་", "ཏམ་",
                 "པ", "བ",
                 "པ་", "བ་",
                 "པོ", "བོ",
                 "པོ་", "བོ་",
                 "དོ", "བོ", "འོ", "ཏོ",
                 "དོ་", "བོ་", "འོ་", "ཏོ་"]

    for p in pattern.parts():
        string = p.tostring(decoder)

        puncts = ['།', '_']
        if [True for a in puncts if a in string]:
            parts_punct += 1

        elif string not in to_ignore:
            parts_content += 1

        parts_total += 1

    if mode == 'flex':
        return parts_content >= parts_total // 2
    if mode == 'skip':
        # find how many skips in pattern
        for _, s in pattern.gaps():
            skips += s
        return skips < parts_content + parts_punct
    else:
        raise ValueError('either "flex" or "skip"')


def get_flexgrams(items):
    mode = 'freq'
    top = 0
    t = 0
    flexs = {}
    for pattern, indices in items:
        pattern = encoder.buildpattern(pattern)
        print(pattern.tostring(decoder))
        if top and t >= top:
            break

        if pattern.category() == colibricore.Category.FLEXGRAM:
            if is_interesting_skipgram(pattern):
                parts = list(pattern.parts())
                instances = defaultdict(list)
                count = 0
                for index in indices:
                    # loop over the patterns found at the given index
                    for ppattern in skip.getreverseindex(index):
                        if ppattern.category() == colibricore.Category.SKIPGRAM:
                            pparts = list(ppattern.parts())
                            if parts == pparts:  # find a better way to do it. maybe using subsuming relations
                                instances[skip.getinstance(index, ppattern)].append(index)
                                count += 1

                if mode == 'freq':
                    for k, v in instances.items():
                        instances[k] = len(v)

                flexs[(count, pattern)] = instances
                print(count, end=' ')
                t += 1
    return flexs


def get_flexgrams_multithreads(item):
    mode = 'freq'
    pattern, indices = item
    if pattern == 'ཟེར་ {**} –ར་ {**} ལ་':
        print('ok')
    pattern = encoder.buildpattern(pattern)

    instances = defaultdict(list)
    count = 0
    if pattern.category() == colibricore.Category.FLEXGRAM:
        if is_interesting_skipgram(pattern):
            print(pattern.tostring(decoder), end=' ')
            parts = list(pattern.parts())

            for index in indices:
                # loop over the patterns found at the given index
                for ppattern in skip.getreverseindex(index):
                    if ppattern.category() == colibricore.Category.SKIPGRAM:
                        pparts = list(ppattern.parts())
                        if parts == pparts:  # find a better way to do it. maybe using subsuming relations
                            instances[skip.getinstance(index, ppattern)].append(index)
                            count += 1

            if mode == 'freq':
                for k, v in instances.items():
                    instances[k] = len(v)

            print(count)

            return tuple((count, pattern)), instances
        else:
            return (-1, -1), False
    else:
        return (-1, -1), False


if __name__ == '__main__':
    gram = 10
    cores = 4
    name = '100karmas_seg'
    corpus_path = '../output/segmented/stories_raw'
    modelsuffix = f'_{gram}'
    maxgram = gram
    outname = f'flexgrams{gram}.txt'


    # 1. Prepare corpus
    encoder, decoder, names = prepare_corpus(corpus_path, name)

    # 2. build skipgram model
    skip = skipgram_model(name + modelsuffix, names['encodedcorpus'], 2, maxgram)

    # 3. Compute flexgrams
    skip.computeflexgrams_fromskipgrams()

    # 4. find flexgrams
    data = [(p.tostring(decoder), list(i)) for p, i in skip.items()]
    flexs = {}
    with Pool(cores) as p:
        flexs.update(p.map(get_flexgrams_multithreads, data))

    getter = itemgetter(1)
    with Path(outname).open('w') as f:
        for key in sorted(flexs.keys(), reverse=True):
            if key == (-1, -1):
                continue
            freq, pattern = key
            f.write(f'{freq} {pattern.tostring(decoder)}\n')

            instances = OrderedDict(sorted(flexs[key].items(), key=getter, reverse=True))
            for k, v in instances.items():
                f.write(f'\t{k.tostring(decoder)} {v}\n')
            f.write('\n')
